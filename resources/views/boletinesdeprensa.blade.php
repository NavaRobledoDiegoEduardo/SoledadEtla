<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.links')
<body>

  <!-- ======= menu ======= -->
  <header id="header" class="fixed-top d-flex align-items-center ">
   @include('layouts.menu')
 </header>
  <!-- End menu -->

  <main id="main">

    <div class="directorio section-title" data-aos="fade-up">
      <h2>¡Enterate!</h2>
    </div>

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-8 entries">

            @foreach($boletines as $boletin)
            <article class="entry">

              <div class="entry-img">
                <img src="{{$boletin->url_imagen}}" alt="" class="img-fluid">
              </div>

              <h2 class="entry-title">
                <a >{{$boletin->titulo}}</a>
              </h2>

              <div class="entry-meta">
                <ul>
                  <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a><time >{{$boletin->created_at}}</time></a></li>
                </ul>
              </div>

              <div class="entry-content">
                <p>{{$boletin->parrafo}} </p>

              </div>

            </article><!-- End blog entry -->

  @endforeach




              {!!$boletines->links()!!}


          </div><!-- End blog entries list -->

          <div class="col-lg-4">

            <div class="sidebar">

            <div id="fb-root"></div>
            <div class="fb-page" data-href="https://www.facebook.com/Angel-Yoshimar-Cruz-Melchor-101241965473755" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Angel-Yoshimar-Cruz-Melchor-101241965473755" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Angel-Yoshimar-Cruz-Melchor-101241965473755">Angel Yoshimar Cruz Melchor</a></blockquote></div>
            </div><!-- End sidebar -->

          </div><!-- End blog sidebar -->

        </div>

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
   @include('layouts.footer')
  <!-- End Footer -->
  <!-- Vendor JS Files -->
@include('layouts.scritps')
</body>

</html>
