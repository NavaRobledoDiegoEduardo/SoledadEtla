<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.links')
<body>

  <!-- ======= menu ======= -->
  <header id="header" class="fixed-top d-flex align-items-center header-transparent">
   @include('layouts.menu')
 </header>
  <!-- End menu -->

  <!-- ======= Seccion carousel ======= -->
  <section id="hero" class="d-flex justify-cntent-center align-items-center">
    <div id="heroCarousel" class="container carousel carousel-fade" data-bs-ride="carousel" data-bs-interval="5000">

      <!-- Slides-->
        @foreach($boletines as $boletin)
      <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
        <div class="carousel-container">
          <h2 class="animate__animated animate__fadeInDown">{{$boletin->titulo}}</h2>
          <p class="animate__animated animate__fadeInUp">{{$boletin->descripcion}}</p>
          <a href="" class="btn-get-started animate__animated animate__fadeInUp">Ver mas</a>
        </div>
      </div>
        @endforeach



      <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
        <span class="carousel-control-prev-icon bx bx-chevron-left" aria-hidden="true"></span>
      </a>

      <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
        <span class="carousel-control-next-icon bx bx-chevron-right" aria-hidden="true"></span>
      </a>

    </div>
  </section><!-- End seccion carusel -->

  <main id="main">
    <div class="section-title" data-aos="fade-up">
      <h2>Boletines de prensa</h2>
    </div>
    <!-- ======= Seccion boletines de prensa ======= -->
    <section class="margen service-details">
      <div class="container">

        <div class="row">
          @foreach($boletines as $boletines)
          <div class="col-md-4 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="{{$boletines->url_imagen}}"  alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a>{{$boletines->titulo}}</a></h5>
                <p class="card-text">{{$boletines->descripcion}}</p>
                <div class="read-more"><a href="#"><i class="bi bi-arrow-right"></i>Mas detalles</a></div>
              </div>
            </div>
          </div>
            @endforeach

              </div>
      </div>
    </section> <!--end seccion boletines-->
    <section class="margen pricing" data-aos="fade-up">
            <div class="col text-center">
       <a href="{{ url('/boletines') }}" class="get-started-btn">Ver todos</a>
  </div>

  <div class="temas section-title">
    <h2>Temas de interés público</h2>
  </div>
    </section>



    <!-- ======= Services Section ======= -->
    <section class="services">
      <div class="container">

        <div class="row">
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up">
            <div class="icon-box icon-box-pink">
              <div class="icon"><i class="bx bx-alarm"></i></div>
              <h4 class="title"><a href="{{ url('/horariosservicios') }}">Horarios de recaudación de rentas y servicios</a></h4>
              <p class="description">¡Recuerda pagar tus contribuciones municipales! consulta aqui las fechas.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="icon-box icon-box-cyan">
              <div class="icon"><i class="bx bx-donate-heart"></i></div>
              <h4 class="title"><a href="">Programas Sociales</a></h4>
              <p class="description">Consulta aqui los diferentes tipos de apoyo para la ciudadania.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box icon-box-green">
              <div class="icon"><i class="bx bx-group"></i></div>
              <h4 class="title"><a href="">Padrón de beneficiarios</a></h4>
              <p class="description">Consulta el listado de aquellos beneficiarios de programas sociales.</p>
            </div>
          </div>




          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box icon-box-green">
              <div class="icon"><i class="bx bx-football"></i></div>
              <h4 class="title"><a href="">Arte,Tradición y Deporte</a></h4>
              <p class="description">¡Enterate de las diferentes actividades culturales y deportivas!</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box icon-box-pink">
              <div class="icon"><i class="bx bx-female"></i></div>
              <h4 class="title"><a href="">Estancia de la mujer</a></h4>
              <p class="description">Contribuir en el pleno desarrollo de las mujeres en la sociedad.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box icon-box-blue">
              <div class="icon"><i class="bx bxs-megaphone"></i></div>
              <h4 class="title"><a href="">Sesiones de cabildo</a></h4>
              <p class="description">¡Enterate!</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="icon-box icon-box-cyan">
              <div class="icon"><i class="bx bx-book-bookmark"></i></div>
              <h4 class="title"><a href="">Leyes,reglamentos y código de ética</a></h4>
              <p class="description">Consulta aqui leyes,reglamentos y código de ética de los servidores publicos.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box icon-box-blue">
              <div class="icon"><i class="bx bxs-book"></i></div>
              <h4 class="title"><a href="">Plan municipal</a></h4>
              <p class="description">Conoce proyectos,actividades,y plan de desarollo 2022-2024.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Why Us Section ======= -->
    <section class="why-us section-bg" data-aos="fade-up" date-aos-delay="200">
      <div class="container">

        <div class="row">
          <div class="col-lg-6 video-box">
            <img src="assets/img/why-us.jpg" class="img-fluid" alt="">
            <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
          </div>

          <div class="col-lg-6 d-flex flex-column justify-content-center p-5">

            <div class="icon-box">
              <div class="icon"><i class="bx bx-chat"></i></div>
              <h4 class="title"><a href="">Mensaje del presidente</a></h4>
              <p class="description">Por el desarollo de Soledad Etla,sus agencias y parajes,un verdadero cambio.</p>
            </div>



          </div>
        </div>

      </div>
    </section><!-- End Why Us Section -->


  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
   @include('layouts.footer')
  <!-- End Footer -->
  <!-- Vendor JS Files -->
@include('layouts.scritps')
</body>

</html>
