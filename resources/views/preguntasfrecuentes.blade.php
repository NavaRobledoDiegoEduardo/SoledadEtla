<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.links')
<body>

  <!-- ======= menu ======= -->
  <header id="header" class="fixed-top d-flex align-items-center ">
   @include('layouts.menu')
 </header>
  <!-- End menu -->


    <main id="main">



      <!-- ======= Features Section ======= -->
      <section class="features">
        <div class="container">

          <div class="section-title">
            <h2>Preguntas Frecuentes</h2>
          </div>

          <div class="row  " data-aos="fade-up">
            <div class="col-md-5 margenpreguntas">
              <img src="assets/img/pregunta1.svg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-4">
              <p>1.-¿Cuál es la finalidad del micrositio del H.Ayuntamiento de Soledad Etla?</p>
              <p class="justificar fst-italic">
                Crear un vínculo entre los habitantes de Soledad Etla,agencias,parajes y el H.Ayuntamiento de Soledad Etla para atender problemáticas de desarrollo a través de la interacción y colaboración proactiva de ambas partes.
              </p>
            </div>
          </div>

          <div class="row "  data-aos="fade-up">
            <div class="col-md-5 order-1 order-md-2">
              <img src="assets/img/pregunta2.svg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5 order-2 order-md-1">
              <p>2.-¿En qué beneficia a la ciudadanía el micrositio del H.Ayuntamiento de Soledad Etla?</p>
              <p class="justificar fst-italic">
                En fortalecer los espacios de diálogo entre los habitantes de Soledad Etla,agencias,parajes y el H.Ayuntamiento de Soledad Etla, en donde el ciudadano podrá expresar lo que a su interés convenga, así como, aportar propuestas y soluciones respecto a algún problema de interés público, de igual forma, en la obtención de información actualizada de manera inmediata, entre otros servicios.
              </p>
            </div>
          </div>

          <div class="row " data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/pregunta3.svg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <p>3.-¿Es seguro proporcionar mis datos personales en caso de requerir información y/o realizar un trámite municipal?<p>
              <p class="justificar fst-italic">La Constitución Política de los Estados Unidos Mexicanos, en sus artículos 6°, apartado A y 16 segundo párrafo; garantizan la protección de datos personales y, por lo tanto, se deberá garantizar la protección y tratamiento de los mismos.
De acuerdo a la Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados, se deberán establecer y mantener las medidas de seguridad de carácter administrativo, físico y técnico para la protección de los datos personales, que permitan protegerlos contra daño, pérdida, alteración, destrucción o su uso, acceso o tratamiento no autorizado, así como garantizar su confidencialidad, integridad y disponibilidad.
              </p>

            </div>
          </div>

          <div class="row " data-aos="fade-up">
            <div class="col-md-5 order-1 order-md-2">
              <img src="assets/img/pregunta4.svg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5 order-2 order-md-1">
              <p>4.-¿Cuánto tiempo tiene el área municipal en dar respuesta a mi solicitud de información o trámite?</p>
              <p class="justificar fst-italic">
                La respuesta a la solicitud o trámite deberá ser notificada al interesado en el menor tiempo posible, para brindar un mejor servicio, el tiempo de respuesta estara sujeto al trámite o información solicitada.
              </p>

            </div>
          </div>

        </div>
      </section><!-- End Features Section -->

    </main><!-- End #main -->

  <!-- ======= Footer ======= -->
   @include('layouts.footer')
  <!-- End Footer -->
  <!-- Vendor JS Files -->
@include('layouts.scritps')
</body>

</html>
