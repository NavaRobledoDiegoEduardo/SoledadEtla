<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.links')
<body>

  <!-- ======= menu ======= -->
  <header id="header" class="fixed-top d-flex align-items-center ">
   @include('layouts.menu')
 </header>
  <!-- End menu -->
  <main id="main">

    <div class=" directorio section-title">
      <h2>Buzón virtual</h2>
      <p>Herramienta puesta al alcance de la ciudadanía por el municipio de Soledad Etla para solicitas información. Lo anterior con la finalidad de ofrecerte un mejor servicio.</p>
    </div>

    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfoWeDJ9-icBRx6Ms3zRnDyTRfw0iT7PUVSiRtthztvmkPM1A/viewform?embedded=true" width="100%" height="943" frameborder="0" marginheight="0" marginwidth="0">Cargando…</iframe>
</main>

  <!-- ======= Footer ======= -->
   @include('layouts.footer')
  <!-- End Footer -->
  <!-- Vendor JS Files -->
@include('layouts.scritps')
</body>

</html>
