  <!-- ======= menu ======= -->

    <div class="container d-flex justify-content-between align-items-center">

      <div class="logo" >
        <img src="{{ 'assets/img/logo.png' }}"   alt="" >

        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="active " href="{{ url('/') }}">Inicio</a></li>
          <li><a href="{{ url('/directorio') }}">Directorio Municipal</a></li>
          <li><a href="about.html">Transparencia</a></li>
            <li><a href="{{ url('/boletines')}}">Convocatorias y Anuncios</a></li>
          <li class="dropdown"><a href="#"><span>Tramites y Servicios</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="https://www.gob.mx/ActaNacimiento/">Acta de Nacimiento</a></li>
              <li><a href="https://www.gob.mx/curp/">Curp</a></li>
              <li><a href="https://www.gob.mx/curp/">Constancia de Residencia</a></li>
              <li><a href="https://www.gob.mx/curp/">Constancia de Origen y Vecindad</a></li>
              <li><a href="https://www.gob.mx/curp/">Constancia de Identidad</a></li>
            </ul>
          </li>
                    <li><a href="{{ url('/atencionciudadana')}}">Contacto</a></li>
                    <li><a href="#">Galería</a></li>
          <li>
            @if (Route::has('login'))
                    @auth
                        <a  href="{{ url('/home') }}">Home</a>
                    @else
                       <a  href="{{ route('login') }}">Iniciar Sesión</a>
                    @endauth
            @endif
          </li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  <!--end menu -->
