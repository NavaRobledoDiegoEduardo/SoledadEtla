<footer id="footer" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">


  <div class="footer-top">
    <div class="container">
      <div class="row">

        <div class="col-lg-3 col-md-6 footer-links">
          <h4>Te podria interesar</h4>
          <ul>
            <li><i class="bx bx-chevron-right"></i> <a href="http://www.inafed.gob.mx/work/enciclopedia/EMM20oaxaca/municipios/20539a.html#:~:text=Oaxaca%20%2DSoledad%20Etla&text=Se%20le%20denomina%20Soledad%20en,frijol%20y%20tla%3A%20lugar%20de.&text=S%C3%B3lo%20se%20sabe%20que%20este,que%20proven%C3%ADan%20de%20Guadalupe%20Etla.&text=Impuls%C3%B3%20la%20repartici%C3%B3n%20de%20tierras%20en%20este%20municipio.">Acerca de Soledad Etla</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="{{ url('/preguntasfrecuentes') }}">Preguntas frecuentes</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="http://www.microrregiones.gob.mx/catloc/LocdeMun.aspx?tipo=clave&campo=loc&ent=20&mun=539">Información Estadistica</a></li>

          </ul>
        </div>

        <div class="col-lg-3 col-md-6 footer-links">
          <h4>Servicios</h4>
          <ul>
            <li><i class="bx bx-chevron-right"></i> <a href="https://www.gob.mx/curp/">Curp</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="https://www.gob.mx/ActaNacimiento/">Acta de Nacimiento</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Constancia de Origen y Vecindad</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Constancia de Identidad</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Constancia de Residencia</a></li>
          </ul>
        </div>

        <div class="col-lg-3 col-md-6 footer-contact">
          <h4>Contacto</h4>
          <p>
            Calle Libertad #2 68250<br>
            Soledad Etla<br>
            Oaxaca de juarez,Oaxaca <br><br>
            <strong>Telefono:</strong> 951 521 4960<br>
            <strong>Email:</strong> info@example.com<br>
          </p>

        </div>

        <div class="col-lg-3 col-md-6 footer-info">
          <h3>Acerda de</h3>
          <p>parrafo de que es el municipio</p>
          <div class="social-links mt-3">
            <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
            <!--
            <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
            <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
            <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>-->
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="container">
    <div class="copyright">
      &copy; Copyright <strong><span>H.AyuntamientoSoledadEtla</span></strong>.Todos los derechos reservados.
    </div>
    <div class="credits">
      Designed by <a href="#">H.AyuntamientoSoledadEtla</a>
    </div>
  </div>
</footer><!-- End Footer -->
<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
