@extends('adminlte::page')
@section('plugins.Datatables', true)
@section('title', 'Usuarios')
@section('content_header')
    <h1 align="center">Usuarios</h1>
@stop
@section('content')
@if(Session::has('Mensaje'))
<div class="alert alert-success">{{Session::get('Mensaje')}}</div>
@endif

<!-- Boton Agregar Usuario -->

<button type="button" class="btn btn-success btn-sm" style="margin-bottom:20px" data-toggle="modal" data-target="#modalAgregar">
  + Agregar
</button>

<!-- Contenido del Modal -->

<div class="modal fade bd-example-modal-lg" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form method="POST" action="{{url('registrar')}}" enctype="multipart/form-data">
      {{csrf_field()}}
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel" >Registrar usuario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-row">
            <div class="col-md-4 mb-3">
              <label for="name1">Nombre(s)</label>
              <input type="text" class="form-control" id="name1" name="name1" onkeypress="return soloLetras(event)" placeholder="Nombre" required>
            </div>
            <div class="col-md-4 mb-3">
              <label for="apellido_paterno1">Apellido paterno</label>
              <input type="text" class="form-control" id="apellido_paterno1" name="apellido_paterno1" onkeypress="return soloLetras(event)" placeholder="Apellido paterno"  required>
            </div>
            <div class="col-md-4 mb-3">
              <label for="apellido_materno1">Apellido materno</label>
              <input type="text" class="form-control" id="apellido_materno1" name="apellido_materno1" onkeypress="return soloLetras(event)" placeholder="Apellido materno">
            </div>
          </div>
          <hr>

          <div class="form-row">
            <div class="col-sm">
              <label for="direccion1">Dirección</label>
              <input type="text" class="form-control" id="direccion1" name="direccion1" placeholder="Dirección">
            </div>
            <div class="col-sm">
              <label for="horario1">Horario</label>
              <input type="text" class="form-control" id="horario1" name="horario1" placeholder="Horario laboral">
            </div>
            <div class="col-sm">
              <label for="password1">Contraseña</label>
              <input type="text" class="form-control" id="password1" name="password1" placeholder="Contraseña">
            </div>
          </div>
          <hr>
          <div class="form-row">
            <div class="col-sm">
              <label for="email1">Email</label>
              <input type="email" class="form-control" id="email1" name="email1" placeholder="Email" aria-describedby="inputGroupPrepend2">
            </div>

            <div class="col-sm">
              <label for="telefono1">Teléfono</label>
              <input type="text" class="form-control" id="telefono1" name="telefono1" onKeyPress="return validanumeros(event)" placeholder="Telefono" aria-describedby="inputGroupPrepend2">
            </div>

            <div class="col-sm">
              <label for="cargo1">Cargo</label>
              <input type="text" class="form-control" id="cargo1" name="cargo1" onKeyPress="return soloLetras(event)" placeholder="Cargo" aria-describedby="inputGroupPrepend2">
            </div>
          </div>
            <hr>
        <div class="form-row">
          <div class="col-sm">
        <label  for="imagen1">Imagen</label>
 <input type="file" class="form-control" id="imagen1" name="imagen1"/>
</div>
      </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <input type="submit" class="btn btn-primary" value="Guardar"></input>
      </div>
      </form>
    </div>
  </div>
</div>





<div class="modal fade bd-example-modal-lg" id="modaleditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form id="form-editar" method="POST" action="" enctype="multipart/form-data">
      {{csrf_field()}}
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel" >Editar usuario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-row">
            <div class="col-md-4 mb-3">
              <label for="name">Nombre(s)</label>
              <input type="text" class="form-control" id="name" name="name" onkeypress="return soloLetras(event)" placeholder="Nombre" required>
            </div>
            <div class="col-md-4 mb-3">
              <label for="apellido_paterno">Apellido paterno</label>
              <input type="text" class="form-control" id="apellido_paterno" name="apellido_paterno" onkeypress="return soloLetras(event)" placeholder="Apellido paterno"  required>
            </div>
            <div class="col-md-4 mb-3">
              <label for="apellido_materno">Apellido materno</label>
              <input type="text" class="form-control" id="apellido_materno" name="apellido_materno" onkeypress="return soloLetras(event)" placeholder="Apellido materno">
            </div>
          </div>
          <hr>

          <div class="form-row">
            <div class="col-sm">
              <label for="direccion">Dirección</label>
              <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección">
            </div>
            <div class="col-sm">
              <label for="horario">Horario</label>
              <input type="text" class="form-control" id="horario" name="horario" placeholder="Horario laboral">
            </div>
            <div class="col-sm">
              <label for="password">Contraseña</label>
              <input type="text" class="form-control" id="password" name="password" placeholder="Contraseña">
            </div>
          </div>
          <hr>
          <div class="form-row">
            <div class="col-sm">
              <label for="email">Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="Email" aria-describedby="inputGroupPrepend2">
            </div>

            <div class="col-sm">
              <label for="telefono">Teléfono</label>
              <input type="text" class="form-control" id="telefono" name="telefono" onKeyPress="return validanumeros(event)" placeholder="Telefono" aria-describedby="inputGroupPrepend2">
            </div>

            <div class="col-sm">
              <label for="cargo">Cargo</label>
              <input type="text" class="form-control" id="cargo" name="cargo" onKeyPress="return soloLetras(event)" placeholder="Cargo" aria-describedby="inputGroupPrepend2">
            </div>
          </div>
            <hr>
        <div class="form-row">
          <div class="col-sm">
        <label  for="imagen">Imagen</label>
 <input type="file" class="form-control" id="imagen" name="imagen"/>
</div>
      </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <input type="submit" class="btn btn-primary" value="Guardar"></input>
      </div>
      </form>
    </div>
  </div>
</div>

<!--Tabla donde se listan los usuarios-->
<div class="table-responsive">
<table id="example" class="table table-striped table-bordered display responsive no-wrap"  >
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Correo</th>
                <th>Telefono</th>
                <th>Cargo</th>
                <th>Dirección</th>
                <th>Horario</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach($usuarios as $usuario)
          <tr>
            <td>{{$usuario->id}}</td>
            <td>{{$usuario->name}} {{$usuario->apellido_paterno}} {{$usuario->apellido_materno}}</td>
            <td>{{$usuario->email}}</td>
            <td>{{$usuario->telefono}}</td>
            <td>{{$usuario->cargo}}</td>
            <td>{{$usuario->direccion}}</td>
            <td>{{$usuario->horario}}</td>
            <td>

              <button type="input" class="btn btn-outline-primary btn-sm user-edit" data-id="{{$usuario->id}}"title="Editar">
                  <span class="fas fa-fw fa-edit"></span>
              </button>
              <form method="post" action="{{route('eliminarusuario',$usuario->id)}}" style="display:inline;">
              {{csrf_field()}}
              {{method_field('DELETE')}}
                <button type="input" class="btn btn-outline-danger btn-sm" title="Eliminar" onClick='return confirm("¿Está seguro que desea eliminar?")'>
                  <span class="fas fa-fw fa-trash-alt"></span>
                </button>
              </form>

            </td>
        </tr>
        @endforeach
        </tbody>

    </table>
</div>


@stop

@section('css')
@stop

@section('js')

<script>
$(document).ready(function(){
    //marca como seleccionada la opcion del menu
    $('#tab5').addClass('tab-select');

    $('#example').DataTable({
       responsive: true,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Todo"]],
        pageLength: 5,
        language: {
            "url": "{{asset('Spanish.json')}}"
        },

    });
});

function soloLetras(e) {
  key = e.keyCode || e.which;
  tecla = String.fromCharCode(key).toLowerCase();
  letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
  especiales = [8, 37, 39, 46];

  tecla_especial = false
  for(var i in especiales) {
      if(key == especiales[i]) {
          tecla_especial = true;
          break;
      }
  }

  if(letras.indexOf(tecla) == -1 && !tecla_especial)
      return false;
}
function validanumeros(e){
  key = e.keyCode || e.which;
  teclado = String.fromCharCode(key).toLowerCase();
   numeros = "0123456789";
   especiales = [8, 37, 39, 46];
   teclado_especial = false;

   for(var i in especiales){
     if(key==especiales[i]){
       teclado_especial = true;
       break;
     }
   }
   if(numeros.indexOf(teclado)== -1 && !teclado_especial){
     return false;
   }

}
$(".user-edit").click(function(){
    var id = $(this).data('id');
    $("#name").val("");
    //$("#role").val("");
  $.ajax({
        url:'/user/'+id,
        type:'get',
        success:  function (response){
            if(response.status == 200){
                $("#name").val(response.user.name);
                $("#apellido_materno").val(response.user.apellido_materno);
                $("#apellido_paterno").val(response.user.apellido_paterno);
                $("#email").val(response.user.email);
                $("#telefono").val(response.user.telefono);
                $("#cargo").val(response.user.cargo);
                $("#direccion").val(response.user.direccion);
                $("#horario").val(response.user.horario);
                $("#password").val(response.user.password);
                  $("#form-editar").prop("action","/actualizarusuario/"+response.user.id);
            }else{
                alert('Usuario no encontrado!!!');
            }
        },
        error:function(x,xs,xt){
            alert('Ha ocurrido un problema, intente mas tarde');
        }
    });
  $('#modaleditar').modal('show');
});

</script>

@stop
