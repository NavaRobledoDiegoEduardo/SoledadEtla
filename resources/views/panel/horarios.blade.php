@extends('adminlte::page')
@section('plugins.Datatables', true)
@section('title', 'Boletines')
@section('content_header')
    <h1 align="center">Anuncios</h1>
@stop
@section('content')
@if(Session::has('Mensaje'))
<div class="alert alert-success">{{Session::get('Mensaje')}}</div>
@endif

<!-- Boton Agregar Usuario -->

<button type="button" class="btn btn-success btn-sm" style="margin-bottom:20px" data-toggle="modal" data-target="#modalAgregar">
  + Agregar
</button>

<!-- Contenido del Modal -->

<div class="modal fade bd-example-modal-lg" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form method="POST" action="{{url('registrarhorario')}}" enctype="multipart/form-data">
      {{csrf_field()}}
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel1" >Registrar Horario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-row">
            <div class="col-sm">
              <label for="titulo1">Titulo</label>
              <input type="text" class="form-control" id="titulo1" name="titulo1"  placeholder="Titulo" required>
            </div>
            <div class="col-sm">
              <label for="descripcion1">Descripción</label>
              <input type="text" class="form-control" id="descripcion1" name="descripcion1"  placeholder="Descripción"  required>
            </div>

          </div>
            <hr>
        <div class="form-row">
          <div class="col-sm">
            <label for="hora1">Hora</label>
            <input class="form-control" id="hora1" name="hora1" placeholder="Rango horas" required>
        </div>
        <div class="col-sm">
          <label for="hora1">Fecha</label>
          <input class="form-control" id="fecha1" name="fecha1" placeholder="Rango fechas" required>
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <input type="submit" class="btn btn-primary" value="Guardar"></input>
      </div>
      </form>
    </div>
  </div>
</div>





<div class="modal fade bd-example-modal-lg" id="modaleditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form id="form-editar" method="POST" action="" enctype="multipart/form-data">
      {{csrf_field()}}
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel" >Editar horario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-row">
            <div class="col-sm">
              <label for="titulo">Titulo</label>
              <input type="text" class="form-control" id="titulo" name="titulo"  placeholder="Titulo" required>
            </div>
            <div class="col-sm">
              <label for="descripcion">Descripción</label>
              <input type="text" class="form-control" id="descripcion" name="descripcion"  placeholder="Descripción"  required>
            </div>
                  </div>
            <hr>
        <div class="form-row">
          <div class="col-sm">
            <label for="hora">Hora</label>
            <input class="form-control" id="hora" name="hora" placeholder="Rango horas" required>
        </div>
        <div class="col-sm">
          <label for="hora">Fecha</label>
          <input class="form-control" id="fecha" name="fecha" placeholder="Rango fechas" required>
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <input type="submit" class="btn btn-primary" value="Guardar"></input>
      </div>
      </form>
    </div>
  </div>
</div>

<!--Tabla donde se listan los usuarios-->
<div class="table-responsive">
<table id="example" class="table table-striped table-bordered display responsive no-wrap"  >
        <thead>
            <tr>
                <th>Id</th>
                <th>Servicio</th>
                <th>Descripción</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach($horarios as $horario)
          <tr>
            <td>{{$horario->id}}</td>
            <td>{{$horario->nombre}}</td>
            <td>{{$horario->descripcion}}</td>
            <td>{{$horario->fecha}}</td>
            <td>{{$horario->hora}}</td>

            <td>

              <button type="input" class="btn btn-outline-primary btn-sm horario-edit" data-id="{{$horario->id}}"title="Editar">
                  <span class="fas fa-fw fa-edit"></span>
              </button>
              <form method="post" action="{{route('eliminarhorario',$horario->id)}}" style="display:inline;">
              {{csrf_field()}}
              {{method_field('DELETE')}}
                <button type="input" class="btn btn-outline-danger btn-sm" title="Eliminar" onClick='return confirm("¿Está seguro que desea eliminar?")'>
                  <span class="fas fa-fw fa-trash-alt"></span>
                </button>
              </form>

            </td>
        </tr>
        @endforeach
        </tbody>

    </table>
</div>


@stop

@section('css')
@stop

@section('js')

<script>
$(document).ready(function(){
    //marca como seleccionada la opcion del menu
    $('#tab5').addClass('tab-select');

    $('#example').DataTable({
       responsive: true,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Todo"]],
        pageLength: 5,
        language: {
            "url": "{{asset('Spanish.json')}}"
        },

    });
});

function soloLetras(e) {
  key = e.keyCode || e.which;
  tecla = String.fromCharCode(key).toLowerCase();
  letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
  especiales = [8, 37, 39, 46];

  tecla_especial = false
  for(var i in especiales) {
      if(key == especiales[i]) {
          tecla_especial = true;
          break;
      }
  }

  if(letras.indexOf(tecla) == -1 && !tecla_especial)
      return false;
}
function validanumeros(e){
  key = e.keyCode || e.which;
  teclado = String.fromCharCode(key).toLowerCase();
   numeros = "0123456789";
   especiales = [8, 37, 39, 46];
   teclado_especial = false;

   for(var i in especiales){
     if(key==especiales[i]){
       teclado_especial = true;
       break;
     }
   }
   if(numeros.indexOf(teclado)== -1 && !teclado_especial){
     return false;
   }

}
$(".horario-edit").click(function(){
    var id = $(this).data('id');
  $.ajax({
        url:'/horario/'+id,
        type:'get',
        success:  function (response){
            if(response.status == 200){
                $("#titulo").val(response.horario.nombre);
                $("#descripcion").val(response.horario.descripcion);
                $("#fecha").val(response.horario.fecha);
                $("#hora").val(response.horario.hora);
                  $("#form-editar").prop("action","/actualizarhorario/"+response.horario.id);
            }else{
                alert('horario no encontrado!!!');
            }
        },
        error:function(x,xs,xt){
            alert('Ha ocurrido un problema, intente mas tarde');
        }
    });
  $('#modaleditar').modal('show');
});

</script>

@stop
