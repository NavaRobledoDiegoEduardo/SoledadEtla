
<!DOCTYPE html>

      @section('plugins.Datatables', true)
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.links')


<body>

  <!-- ======= menu ======= -->
  <header id="header" class="fixed-top d-flex align-items-center ">
   @include('layouts.menu')
 </header>
  <!-- End menu -->



    <main id="main">

        <div class="margenes">
      <table id="horarios" class="table table-striped table-bordered table-sm" style="width:100%">
        <thead>
          <tr>
            <th>Servicio</th>
            <th>Fecha</th>
            <th>Hora</th>
            <th>Descripción</th>
          </tr>
        </thead>
        <tbody>
          @foreach($horariosservicios as $horariosservicios)
          <tr>
            <td>{{$horariosservicios->nombre}}</td>
            <td>{{$horariosservicios->fecha}}</td>
            <td>{{$horariosservicios->hora}}</td>
            <td>{{$horariosservicios->descripcion}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
</div>

    </main><!-- End #main -->

  <!-- ======= Footer ======= -->
   @include('layouts.footer')
  <!-- End Footer -->
  <!-- Vendor JS Files -->
@include('layouts.scritps')
</body>
<script>



$(document).ready(function(){
    $('#horarios').DataTable({
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Todo"]],
        pageLength: 5,
        language: {
            "url": "{{asset('Spanish.json')}}"
        }
    });
});


</script>
</html>
