<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.links')
<body>

  <!-- ======= menu ======= -->
  <header id="header" class="fixed-top d-flex align-items-center ">
   @include('layouts.menu')
 </header>
  <!-- End menu -->

  <main id="main">
    <div class="directorio section-title" data-aos="fade-up">
      <h2>Directorio de servidores públicos</h2>
    </div>

    <!-- ======= Team Section ======= -->
    <section class="team" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
      <div class="container">

        <div class="row">

@foreach($cabildo as $cabildo)
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <div class="member-img">
                <img src="{{$cabildo->url_imagen}}" class="img-fluid" alt="">
                <img src="{{ 'assets/img/fotosdirectorio/H.Ayuntamiento.png' }}" class="img-fluid proteger" alt="" >
              </div>
              <div class="member-info">
                <h4>{{$cabildo->name}} {{$cabildo->apellido_paterno}} {{$cabildo->apellido_materno}}</h4>
                <span>{{$cabildo->cargo}}</span>
                <p class="textos"><i class="bx bx-phone-call"></i> Telefono:{{$cabildo->telefono}}</p>
                <p class="textos"><i class="bx bx-envelope"></i> Correo:{{$cabildo->email}}</p>

              </div>
            </div>
          </div>

            @endforeach

        </div>

      </div>
    </section><!-- End Team Section -->

  </main><!-- End #main -->


  <!-- ======= Footer ======= -->
   @include('layouts.footer')
  <!-- End Footer -->
  <!-- Vendor JS Files -->
@include('layouts.scritps')
</body>

</html>
