<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

      $admin = User::create([
        'name' => 'Diego Eduardo',
        'apellido_paterno' => 'Nava',
        'apellido_materno' => 'Robledo',
        'email' => 'navard.2047@gmail.com',
        'password' => Hash::make('c0m345y0u4r3'),
        'cargo' => 'Ing.Sistemas computacionales',
        'direccion' => 'Allende #5 Soledad Etla',
        'horario' => 'Lunes a viernes 17:00 a 20:00 horas',
        'telefono' => '9512206586',
        'url_imagen' => '',
      ]);
      /*
      $cabildo = User::create([
        'name' => 'Karla',
        'apellido_paterno' => 'Gonzalez',
        'apellido_materno' => 'Celis',
        'email' => 'example@gmail.com',
        'password' => Hash::make('1234'),
        'cargo' => 'Prueba',
        'direccion' => 'Allende #5 Soledad Etla',
        'horario' => 'Lunes a viernes 17:00 a 20:00 horas',
        'telefono' => '9512206586',
        'url_imagen' => '',
      ]);
*/
      Role::create(['name' => 'administrador']);
      Role::create(['name' => 'usuario']);
      /*
      $permissionstramites = [];
      $permissions = [];

      $view_tramites = Permission::create(['name' => 'ver-tramites']);
      $edit_tramites = Permission::create(['name' => 'editar-tramites']);

      array_push($permissionstramites,$view_tramites);
      array_push($permissionstramites,$edit_tramites);
      array_push($permissions,$view_tramites);
      array_push($permissions,$edit_tramites);
      array_push($permissions,Permission::create(['name' => 'eliminar-tramites']));
      array_push($permissions,Permission::create(['name' => 'ver-usuarios']));
      array_push($permissions,Permission::create(['name' => 'editar-usuarios']));
      array_push($permissions,Permission::create(['name' => 'crear-usuarios']));
      array_push($permissions,Permission::create(['name' => 'eliminar-usuarios']));
      //array_push($permissions,Permission::create(['name' => 'ver-roles']));
      $role_admin->syncPermissions($permissions);
      $role_cabildo->givePermissionTo($permissionstramites);
      */
      $admin->assignRole('administrador');
  /*    $cabildo->assignRole('usuario');*/



    }
}
