<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'apellido_paterno', 'apellido_materno', 'email', 'telefono' ,'password','cargo','direccion','horario','url_imagen',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //muestra una imagen en el perfil del usuario
    public function adminlte_image(){
        return 'https://picsum.photos/300/300';
    }

    //Muestra un mensaje en el perfil del usuario
    public function adminlte_desc(){
        return 'Eres una persona geneal!!!';
    }

    //Muestra un boton para redirigir al perfil del usuario
    public function adminlte_profile_url(){
        return 'profile/username';
    }

    //Se agrega otro campo para que funcione como atributo del modelo para obtener el rol del usuario
    //Si tiene mas de un rol solo se muestra uno
    public function getRoleAttribute(){
        return $this->getRoleNames()->first();
    }

  
}
