<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class boletines extends Model
{
  protected $fillable = [
      'titulo', 'descripcion', 'parrafo','url_imagen',
  ];
}
