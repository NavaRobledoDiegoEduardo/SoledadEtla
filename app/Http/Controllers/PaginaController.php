<?php

namespace App\Http\Controllers;
use App\User;
use App\boletines;
use App\horarios_servicios;
use Illuminate\Http\Request;

class PaginaController extends Controller
{

      public function directorio(){
          $cabildo = User::where('telefono','!=',9512206586)->get();
          return view('directoriomunicipal',compact('cabildo'));
      }
      public function boletines(){
        $boletines = boletines::paginate(3);
        return view('boletinesdeprensa',compact('boletines'));
      }


      public function atencionciudadana(){
        return view('atencionciudadana');
      }
      public function preguntasfrecuentes(){
        return view('preguntasfrecuentes');
      }
      public function horariosservicios(){
        $horariosservicios=horarios_servicios::all();
        return view('horariosservicios',compact('horariosservicios'));
      }

}
