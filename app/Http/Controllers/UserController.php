<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use App\User;
use Auth;
class UserController extends Controller
{

  public function usuarios(){
    $id=Auth::user()->id;
    if($id==1){
      $usuarios = User::role('usuario')->get();
      return view('panel.usuarios',compact('usuarios'));
    }else{
            return view('panel.tramites');
    }


  }
  public function getUser($id){
      $user = User::find($id);
      if($user){
          return response()->json(['user'=>$user,'status'=>200]);
      }else{
          return response()->json([[],'status'=>404]);
      }
  }
  public function crear(Request $request){
      $user=User::create([
         'name' => $request['name1'],
         'apellido_paterno'=>$request['apellido_paterno1'],
         'apellido_materno'=>$request['apellido_materno1'],
         'email' => $request['email1'],
         'telefono'=> $request['telefono1'],
         'cargo'=> $request['cargo1'],
         'horario'=> $request['horario1'],
         'direccion'=> $request['direccion1'],
         'password'=> Hash::make($request->password1),
         $imagen = $request->file('imagen1'),
         $nombre = $imagen->getClientOriginalName(),
         'url_imagen'=> 'assets/img/fotosdirectorio/'.$nombre,
      ]);
      $file = $request->file('imagen1');
      $file->move(public_path('assets/img/fotosdirectorio'), $file->getClientOriginalName());
      $user->assignRole('usuario');
      return back()->with('Mensaje','usuario creado correctamente');
  }



  public function actualizar(Request $request, $id){
      $user = User::find($id)->update([
          'name' => $request->name,
          'apellido_paterno' => $request->apellido_paterno,
          'apellido_materno' => $request->apellido_materno,
          'email' => $request->email,
          'cargo' => $request->cargo,
          'horario' => $request->horario,
          'direccion' => $request->direccion,
          'password'=> Hash::make($request->password),
          $imagen = $request->file('imagen'),
          $nombre = $imagen->getClientOriginalName(),
          'url_imagen'=> 'assets/img/fotosdirectorio/'.$nombre,
      ]);

      $file = $request->file('imagen');
      $file->move(public_path('assets/img/fotosdirectorio'), $file->getClientOriginalName());
      return back()->with('Mensaje','Usuario actualizado correctamente');
  }

  public function destroy($id){

      User::where('id',$id)->delete();
      return back()->with('Mensaje','Usuario eliminado correctamente');
  }
}
