<?php

namespace App\Http\Controllers;
use App\horarios_servicios;
use Illuminate\Http\Request;

class ServiciosController extends Controller
{
  public function horariospanel(){
      $horarios = horarios_servicios::latest()->get();
            return view('panel.horarios',compact('horarios'));
  }

  public function crear(Request $request){
      $horario=horarios_servicios::create([
         'nombre' => $request['titulo1'],
         'descripcion'=>$request['descripcion1'],
         'fecha'=>$request['fecha1'],
        'hora'=>$request['hora1'],
      ]);

      return back()->with('Mensaje','Horario creado correctamente');
  }
  public function destroy($id){

      horarios_servicios::where('id',$id)->delete();
      return back()->with('Mensaje','Horario eliminado correctamente');
  }
  public function getHorario($id){
      $horario = horarios_servicios::find($id);
      if($horario){
          return response()->json(['horario'=>$horario,'status'=>200]);
      }else{
          return response()->json([[],'status'=>404]);
      }
  }
  public function actualizar(Request $request, $id){
      $horario = horarios_servicios::find($id)->update([
          'nombre' => $request->titulo,
          'descripcion' => $request->descripcion,
          'fecha' => $request->fecha,
          'hora' => $request->hora,
      ]);

      return back()->with('Mensaje','Horario actualizado correctamente');
  }
}
