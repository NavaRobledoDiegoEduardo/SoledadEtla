<?php

namespace App\Http\Controllers;
use App\boletines;
use Illuminate\Http\Request;

class BoletinesController extends Controller
{
  public function boletinespanel(){
      $boletines = boletines::latest()->get();
            return view('panel.boletines',compact('boletines'));
  }

  public function crear(Request $request){
      $boletin=boletines::create([
         'titulo' => $request['titulo1'],
         'descripcion'=>$request['descripcion1'],
         'parrafo'=>$request['parrafo1'],
         $imagen = $request->file('imagen1'),
         $nombre = $imagen->getClientOriginalName(),
         'url_imagen'=> 'assets/img/boletines/'.$nombre,
      ]);
      $file = $request->file('imagen1');
      $file->move(public_path('assets/img/boletines'), $file->getClientOriginalName());
      return back()->with('Mensaje','boletin creado correctamente');
  }
  public function destroy($id){

      boletines::where('id',$id)->delete();
      return back()->with('Mensaje','Boletin eliminado correctamente');
  }
  public function getBoletin($id){
      $boletin = boletines::find($id);
      if($boletin){
          return response()->json(['boletin'=>$boletin,'status'=>200]);
      }else{
          return response()->json([[],'status'=>404]);
      }
  }
  public function actualizar(Request $request, $id){
      $boletin = boletines::find($id)->update([
          'titulo' => $request->titulo,
          'descripcion' => $request->descripcion,
          'parrafo' => $request->parrafo,
          $imagen = $request->file('imagen'),
          $nombre = $imagen->getClientOriginalName(),
           'url_imagen'=> 'assets/img/boletines/'.$nombre,
      ]);

      $file = $request->file('imagen');
      $file->move(public_path('assets/img/boletines'), $file->getClientOriginalName());
      return back()->with('Mensaje','boletin actualizado correctamente');
  }
}
