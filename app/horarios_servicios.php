<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class horarios_servicios extends Model
{
  protected $fillable = [
      'nombre', 'descripcion', 'fecha','hora',
  ];
}
