<?php
use App\boletines;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
          $boletines = boletines::latest()->take(3)->get();
    return view('welcome',compact('boletines'));
});

Auth::routes();

Route::get('/home', 'UserController@usuarios');

Route::get('/directorio','PaginaController@directorio');
Route::get('/boletines','PaginaController@boletines');
Route::get('/atencionciudadana','PaginaController@atencionciudadana');
Route::get('/preguntasfrecuentes','PaginaController@preguntasfrecuentes');
Route::get('/horariosservicios','PaginaController@horariosservicios');

//crud Usuarios
Route::delete('/eliminarusuario/{id}', 'UserController@destroy')->name('eliminarusuario');
Route::post('/registrar','UserController@crear')->name('registrar');
Route::get('user/{id}','UserController@getUser');
Route::post('actualizarusuario/{id}','UserController@actualizar')->name('actualizar.producto');
//crud boletinespanel
Route::get('/boletinespanel','BoletinesController@boletinespanel');
Route::post('/registrarboletin','BoletinesController@crear')->name('registrarboletin');
Route::delete('/eliminarboletin/{id}', 'BoletinesController@destroy')->name('eliminarboletin');
Route::get('boletin/{id}','BoletinesController@getBoletin');
Route::post('actualizarboletin/{id}','BoletinesController@actualizar')->name('actualizar.boletin');
//crud horariosservicios

Route::get('/horariospanel','ServiciosController@horariospanel');
Route::post('/registrarhorario','ServiciosController@crear')->name('registrarhorario');
Route::delete('/eliminarhorario/{id}', 'ServiciosController@destroy')->name('eliminarhorario');
Route::get('horario/{id}','ServiciosController@getHorario');
Route::post('actualizarhorario/{id}','ServiciosController@actualizar')->name('actualizar.horario');
